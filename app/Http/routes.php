<?php
use PhpImap\Mailbox as ImapMailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//https://github.com/barbushin/php-imap
Route::get('/', function () {
  $mailbox = new PhpImap\Mailbox('{imap.gmail.com:993/imap/ssl}INBOX', 'testebazuka@gmail.com', 'rootgarcia69', __DIR__);
  $mailsIds = $mailbox->searchMailbox('ALL');
  if(!$mailsIds) {
      die('Mailbox is empty');
  }

  $mail = $mailbox->getMail($mailsIds[1]);

  echo $mail->textHtml;
  die();
});

Route::get('/dd', function () {
  $mailbox = new PhpImap\Mailbox('{imap.gmail.com:993/imap/ssl}INBOX', 'testebazuka@gmail.com', 'rootgarcia69', __DIR__);
  $mailsIds = $mailbox->searchMailbox('ALL');
  if(!$mailsIds) {
      die('Mailbox is empty');
  }

  // Get the first message and save its attachment(s) to disk:
  $mail = $mailbox->getMail($mailsIds[1]);

  dd($mail);
  die();
});
